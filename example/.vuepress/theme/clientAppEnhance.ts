import { sync } from "./vuex-router-sync/vuex-router-sync.esm-bundler";
import { store } from "./store";
import { defineClientAppEnhance } from "@vuepress/client";
import types from "./store/types";
import Layout from "./layouts/Layout.vue";

export default defineClientAppEnhance(({ app, router, siteData }) => {
  sync(store, router);

  router.addRoute({ path: "/category/:category?", component: Layout });
  router.addRoute({ path: "/posts/", component: Layout });

  router.beforeResolve((to, from, next) => {
    // If this isn't an initial page load.
    if (to.name) {
      // Start the route progress bar.
      store.commit(types.LOAD_START);
    }

    next();
  });

  router.afterEach(() => {
    // Complete the animation of the route progress bar.
    store.commit(types.LOAD_END);
  });
  app.use(store);
});
