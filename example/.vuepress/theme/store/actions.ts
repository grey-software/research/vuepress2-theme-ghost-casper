import types from "./types";

export default {
  updateSite: ({ commit }, configPayload) =>
    commit(types.SITE_UPDATE, configPayload),
  updatePage: ({ commit }, page) => commit(types.PAGE_UPDATE, page),
  updateParams: ({ commit }, params) => commit(types.ROUTER_PARAMS, params),
  searchInput: ({ commit }, { target }) => {
    commit(types.SEARCH, target.value);
  },
};
